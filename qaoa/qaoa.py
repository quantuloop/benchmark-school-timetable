# Import tools for running QAOA
from ket import around, X, H, ctrl, RZ, RX, quant, dump, quantum_exec_time
from os import environ

# import math tools
import numpy as np

# We import the tools to handle general Graphs
import networkx as nx


def partial_mixer(neighbor, ancilla, target, beta):
    def outer():
        if neighbor == None:
            X(ancilla)
        else:
            with around(X, neighbor):
                ctrl(neighbor, X, ancilla)

    with around(outer):
        with around([H, ctrl(0, X, 1)], target):
            ctrl(ancilla, RZ, 2*beta, target[1])

        with around([RX(-np.pi/2), ctrl(0, X, 1)], target):
            ctrl(ancilla, RZ, 2*beta, target[1])


def neighborhood(G, num_colors, node, color, list_nodes):
    neighbors = list(G[node])
    neighbors_index = [list_nodes.index(neigh) for neigh in neighbors]

    neighbors_color_qubit = [color+(num_colors*u) for u in neighbors_index]

    return neighbors_color_qubit

# Apply the partial mixer for each pair of colors of each node


def mixer(qc, G, beta, num_nodes, num_colors):
    list_nodes = list(G.nodes())
    for u, node in enumerate(G.nodes):
        for i in range(num_colors):
            neighbors_i = neighborhood(G, num_colors, node, i, list_nodes)
            for j in range(num_colors):
                if i < j:
                    neighbors_j = neighborhood(
                        G, num_colors, node, j, list_nodes)
                    neighbors = neighbors_i+neighbors_j

                    if neighbors == []:
                        q_neighbors = None
                    else:
                        q_neighbors = qc[neighbors[0]]
                        for neigh in neighbors[1:]:
                            q_neighbors = q_neighbors + qc[neigh]
                    partial_mixer(
                        q_neighbors,
                        qc[num_nodes*num_colors+u],
                        qc[i+(num_colors*u)] + qc[j+(num_colors*u)],
                        beta)


def phase_separator(qc, gamma, num_nodes, num_colors):
    for node in range(num_colors*num_nodes):
        X(qc[node])
    for k in range(num_colors):
        qubits = [node*num_colors+k for node in range(num_nodes)]
        control = qc[qubits[0]]
        for qub in qubits[1:-1]:
            control = control + qc[qub]
        target = qc[qubits[-1]]
        ctrl(control, RZ, 2*gamma, target)
    for node in range(num_colors*num_nodes):
        X(qc[node])


def qaoa_min_graph_coloring(p, G, num_nodes, num_colors, beta0, gamma, beta):
    # --------------------------
    # Initializing qubits
    # --------------------------
    qc = quant((num_nodes*num_colors) + num_nodes)

    # --------------------------
    # Initial state preparation
    # --------------------------
    coloring = [G.nodes[node]['color'] for node in G.nodes]
    for i, color in enumerate(coloring):
        X(qc[(i*num_colors)+color])

    # --------------------------
    # Alternate application of operators
    # --------------------------
    mixer(qc, G, beta0, num_nodes, num_colors)  # Mixer 0
    for step in range(p):
        phase_separator(qc, gamma[step], num_nodes, num_colors)
        mixer(qc, G, beta[step], num_nodes, num_colors)

    # --------------------------
    # Measurement
    # --------------------------
    #result = measure(qc).get()
    return dump(qc)


def qaoa(par, p, initial_G, num_colors, epsilon, cost_function, school, it_num):
    if ('PBW_EPSILON' in environ and float(environ['PBW_EPSILON']) > epsilon) or 'PBW_EPSILON' not in environ:
        environ['PBW_EPSILON'] = f"{epsilon:f}"
    # --------------------------
    # Unpacking QAOA parameters
    # --------------------------
    beta0 = par[0]
    middle = int(len(par)/2)
    gamma = par[1:middle+1]
    beta = par[middle+1:]
    num_nodes = initial_G.number_of_nodes()

    # --------------------------
    # Verifying Parameters
    # --------------------------
    #print("Using Following parameters: Beta0:", beta0, "Gamma:", gamma, "Beta:", beta)

    # --------------------------
    # Running QAOA on simulator
    # --------------------------
    G = nx.Graph()
    G.add_nodes_from(initial_G)
    G.add_edges_from(initial_G.edges)
    initial_coloring = [initial_G.nodes[node]['color']
                        for node in initial_G.nodes]
    color_graph_from_coloring(G, initial_coloring)

    result = qaoa_min_graph_coloring(
        p, initial_G, num_nodes, num_colors, beta0, gamma, beta)

    #print("Number of States", len(result.get_states()))
    #print("State Vector", result.show('b6:b6:b6:b6:b6:b6'))

    # --------------------------
    # Counting resulting states
    # --------------------------
    counts = {}  # Dictionary for keeping the results of the simulation
    for i, c in result.get_shots(2**20, it_num).items():
        binary = f'{i:0{(num_nodes*num_colors)+num_nodes}b}'
        counts[binary] = c

    # --------------------------
    # Evaluate the data from the simulator
    # --------------------------
    avr_function_value = 0
    for sample in list(counts.keys()):
        try:
            # extracting x to compute f(x)
            # ----------------------------
            x = [int(num) for num in list(sample)]

            # Coloring Graph with counts[sample]
            # ----------------------------------
            coloring = []
            for i in range(len(G)):
                for pos, char in enumerate(x[i*num_colors:(i*num_colors+num_colors)]):
                    if int(char):
                        coloring.append(pos)
            color_graph_from_coloring(G, coloring)

            # Computing fx
            # ------------
            fx = cost_function(G)

            # Compute the expectation value and energy distribution
            # -----------------------------------------------------
            avr_function_value = avr_function_value + counts[sample]*fx
        except IndexError:
            continue

    # Return expected value
    expected_value = avr_function_value/sum(counts.values())
    with open(f'timing/{school}_{p}_{it_num}.txt', 'a') as file:
        file.write(f'{quantum_exec_time():f}, ')

    return expected_value


def color_graph_from_coloring(graph, coloring):
    for index, node in enumerate(graph.nodes):
        graph.nodes[node]['color'] = coloring[index]
    return
