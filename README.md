# Benchmark: School Timetable

Forked from https://gitlab.com/OttoMP/school-timetable. For more details, see the published works:
* O. M. Pires, R. de Santiago and J. Marchi, "*Two Stage Quantum Optimization for the School Timetabling Problem*", 2021 IEEE Congress on Evolutionary Computation (CEC), 2021, pp. 2347-2353, doi: https://doi.org/10.1109/CEC45853.2021.9504701.
* Otto Menegasso Pires, *A quantum heuristic for the School Timetabling Problem*, advisor Rafael de Santiago, co-advisor Jerusa Marchi, PPGCC-UFSC, 2021, https://repositorio.ufsc.br/handle/123456789/231060

## Instances

* `min.py` 56-Qubits
* `den.py` 150-Qubits
* `den-aux.py` 189-Qubits
* `bra.py` 576-Qubits :x: